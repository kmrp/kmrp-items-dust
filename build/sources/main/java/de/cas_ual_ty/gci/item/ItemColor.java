package de.cas_ual_ty.gci.item;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.KMRPTab;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.awt.*;

public class ItemColor extends ItemGCI  {

    private static int DEFAULT_COLOR = Color.LIGHT_GRAY.getRGB();
    private String textureOverlay;

    public ItemColor(String rl, String overlay, @Nullable CreativeTabs tab) {
        super(rl, GunCus.KMRP_TAB);
        this.textureOverlay = overlay;
    }

    private int getColorFromNBT(ItemStack itemStack) {
        NBTTagCompound nbttagcompound = itemStack.getTagCompound();

        if (nbttagcompound == null) {
            return DEFAULT_COLOR;
        }

        NBTTagCompound displayTag = nbttagcompound.getCompoundTag("display");

        if (displayTag == null || !displayTag.hasKey("color")) {
            return DEFAULT_COLOR;
        }

        return displayTag.getInteger("color");
    }


}
