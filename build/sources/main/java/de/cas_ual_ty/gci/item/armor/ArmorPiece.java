package de.cas_ual_ty.gci.item.armor;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;

import java.util.ArrayList;

public class ArmorPiece extends ItemArmor {

    public static ArrayList<ArmorPiece> ARMOR_LIST = new ArrayList<>();
    private final String modelRL;

    public ArmorPiece(String modelRL, ArmorMaterial materialIn, int renderIndexIn, EntityEquipmentSlot equipmentSlotIn) {
        super(materialIn, renderIndexIn, equipmentSlotIn);
        this.modelRL = modelRL;
        this.setUnlocalizedName(GunCus.MOD_ID + ":" + modelRL);
        this.setRegistryName(GunCus.MOD_ID + ":" + modelRL);
        this.setCreativeTab(GunCus.KMRP_TAB);
        this.setMaxDamage(0);
        ArmorPiece.ARMOR_LIST.add(this);
    }

    public String getModelRL()
    {
        return this.modelRL;
    }

}
