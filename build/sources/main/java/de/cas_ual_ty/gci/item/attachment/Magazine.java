package de.cas_ual_ty.gci.item.attachment;

import de.cas_ual_ty.gci.item.ItemToggleable;

public class Magazine extends Attachment
{
	protected int extraCapacity;
	protected float reloadTimeModifier;
	protected String transferableToRl;

	public Magazine(int id, String rl)
	{
		super(id, rl);
		
		this.extraCapacity = 0;
		this.reloadTimeModifier = 1F;
		this.transferableToRl = null;
	}
	
	@Override
	public EnumAttachmentType getType()
	{
		return EnumAttachmentType.MAGAZINE;
	}

	public Magazine setTransferableToRl(String id) {
		this.transferableToRl = id;
		return this;
	}


	public String getTransferableToRl() {
		return this.transferableToRl;
	}
	
	public int getExtraCapacity()
	{
		return this.extraCapacity;
	}
	
	public float getReloadTimeModifier()
	{
		return this.reloadTimeModifier;
	}
	
	public Magazine setExtraCapacity(int extraCapacity)
	{
		this.extraCapacity = extraCapacity;
		return this;
	}
	
	public Magazine setReloadTimeModifier(float reloadTimeModifier)
	{
		this.reloadTimeModifier = reloadTimeModifier;
		return this;
	}
}
