#!/bin/bash
VAR=''
VAR2=''
declare -i y=0
for filename in *.ogg; do
    echo '"'$(basename $filename .ogg)'":{"category":"neutral","sounds":["gci:'$(basename $filename .ogg)'"]},'
done
for filename in *.ogg; do
    f=$(basename $filename .ogg)
    echo 'public static final SoundEventGCI SOUND_'${f^^}'                  = new SoundEventGCI('${y}', "'$(basename $filename .ogg)'");'
    y=$(( y + 1 ))
done

echo "$VAR"
echo "$VAR2"
