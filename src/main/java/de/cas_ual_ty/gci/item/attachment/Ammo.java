package de.cas_ual_ty.gci.item.attachment;

import de.cas_ual_ty.gci.item.ItemGun;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Ammo extends Attachment
{
	protected Item replacementCartridge;
	protected Laser laser;
	protected boolean flashActive;
	protected Integer toggleableToId;
	protected String transferableToRl;

	public Ammo(int id, String rl)
	{
		super(id, rl);
		
		this.replacementCartridge = null;
		this.laser = null;
		this.flashActive = false;
		this.toggleableToId = null;
		this.transferableToRl = null;
	}
	
	public Item getUsedCartrige(ItemStack itemStack, ItemGun itemGun)
	{
		return this.replacementCartridge == null ? itemGun.getCartridge() : this.getReplacementCartridge();
	}
	
/*	@Override
	public boolean shouldRender()
	{
		return true;
	}*/
	
	@Override
	public EnumAttachmentType getType()
	{
		return EnumAttachmentType.AMMO;
	}
	
	public Item getReplacementCartridge()
	{
		return this.replacementCartridge;
	}
	
	public Ammo setReplacementCartridge(Item replacementCartridge)
	{
		this.replacementCartridge = replacementCartridge;
		return this;
	}

	public Ammo setToggleableTo(Integer id) {
		this.toggleableToId = id;
		return this;
	}

	public Integer getToggleableTo() {
		return this.toggleableToId;
	}

	public Ammo setTransferableToRl(String id) {
		this.transferableToRl = id;
		return this;
	}


	public String getTransferableToRl() {
		return this.transferableToRl;
	}

	public Laser getLaser()
	{
		return this.laser;
	}

	public Ammo setLaser(Laser laser)
	{
		this.laser = laser;
		return this;
	}

	public static class Laser
	{
		protected float r;
		protected float g;
		protected float b;

		protected double maxRange;

		protected boolean isBeam;
		protected boolean isPoint;
		protected boolean isRangeFinder;

		public Laser(float r, float g, float b, double maxRange, boolean isBeam, boolean isPoint, boolean isRangeFinder)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.maxRange = maxRange;
			this.isBeam = isBeam;
			this.isPoint = isPoint;
			this.isRangeFinder = isRangeFinder;
		}

		public float getR()
		{
			return this.r;
		}

		public float getG()
		{
			return this.g;
		}

		public float getB()
		{
			return this.b;
		}

		public double getMaxRange()
		{
			return this.maxRange;
		}

		public boolean isBeam()
		{
			return this.isBeam;
		}

		public boolean isPoint()
		{
			return this.isPoint;
		}

		public boolean isRangeFinder()
		{
			return this.isRangeFinder;
		}
	}

	public boolean isFlashActive() {return this.flashActive;}
	public Ammo setFlashActive()
	{
		this.flashActive = true;
		return this;
	}
}
