package de.cas_ual_ty.gci.item.attachment;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;

public class Optic extends Attachment
{
	protected final ResourceLocation overlay;
	
	protected float zoom;
	protected EnumOpticType opticType;

	protected Laser laser;
	protected Integer toggleableToId;
	//protected boolean rangeFinder = false;
	
	public Optic(int id, String rl)
	{
		super(id, rl);
		
		this.overlay = new ResourceLocation(GunCus.MOD_ID, "textures/gui/sights/" + rl + ".png");
		
		this.zoom = 1F;
		this.opticType = EnumOpticType.NORMAL;
		//this.rangeFinder = false;
		this.laser = null;
		this.toggleableToId = null;
	}
	
	@Override
	public EnumAttachmentType getType()
	{
		return EnumAttachmentType.OPTIC;
	}
	
	@Override
	public String getInformationString()
	{
		return I18n.translateToLocal(this.getUnlocalizedName() + ".name").trim() + " §f(" + this.getZoom() + "x)";
	}
	
	public boolean isCompatibleWithMagnifiers()
	{
		return this.zoom <= 4F;
	}
	
	public boolean isCompatibleWithExtraZoom()
	{
		return !this.isCompatibleWithMagnifiers();
	}
	
	public boolean canAim()
	{
		return this.shouldRegister();
	}
	
	public final ResourceLocation getOverlay()
	{
		return this.overlay;
	}
	
	public boolean getIsClosedScope()
	{
		return this.getZoom() >= 6F;
	}
	
	public float getZoom()
	{
		return this.zoom;
	}
	
	public EnumOpticType getOpticType()
	{
		return this.opticType;
	}
	
	public Optic setZoom(float zoom)
	{
		this.zoom = zoom;
		return this;
	}
	
	public Optic setOpticType(EnumOpticType opticType)
	{
		this.opticType = opticType;
		return this;
	}
	
	public static enum EnumOpticType
	{
		NORMAL, NIGHT_VISION, THERMAL; 
	}

	public Optic setToggleableTo(Integer id) {
		this.toggleableToId = id;
		return this;
	}

	public Integer getToggleableTo() {
		return this.toggleableToId;
	}

	public Laser getLaser()
	{
		return this.laser;
	}

	public Optic setLaser(Laser laser)
	{
		this.laser = laser;
		return this;
	}

	public static class Laser
	{
		protected float r;
		protected float g;
		protected float b;

		protected double maxRange;

		protected boolean isBeam;
		protected boolean isPoint;
		protected boolean isRangeFinder;

		public Laser(float r, float g, float b, double maxRange, boolean isBeam, boolean isPoint, boolean isRangeFinder)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.maxRange = maxRange;
			this.isBeam = isBeam;
			this.isPoint = isPoint;
			this.isRangeFinder = isRangeFinder;
		}

		public float getR()
		{
			return this.r;
		}

		public float getG()
		{
			return this.g;
		}

		public float getB()
		{
			return this.b;
		}

		public double getMaxRange()
		{
			return this.maxRange;
		}

		public boolean isBeam()
		{
			return this.isBeam;
		}

		public boolean isPoint()
		{
			return this.isPoint;
		}

		public boolean isRangeFinder()
		{
			return this.isRangeFinder;
		}
	}
	//public boolean hasRangeFinder() {return this.rangeFinder;}
	//public Optic setRangeFinder()
	//{
	//	this.rangeFinder = true;
	//	return this;
	//}
}
