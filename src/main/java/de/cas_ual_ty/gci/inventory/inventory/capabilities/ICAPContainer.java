package de.cas_ual_ty.gci.inventory.inventory.capabilities;

import de.cas_ual_ty.gci.inventory.inventory.ContainerC;

public interface ICAPContainer {

    public void copyInventory(ICAPContainer inventory);
    public ContainerC getInventory();
}