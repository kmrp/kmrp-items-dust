package de.cas_ual_ty.gci.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class DeleteEntityMessage implements IMessage {

    public int playerId;

    public DeleteEntityMessage() {}

    public DeleteEntityMessage(int id) {
        playerId = id;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        buffer.writeInt(playerId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        playerId = buffer.readInt();
    }

    public static class Handler implements IMessageHandler<DeleteEntityMessage, IMessage>
    {
        @Override
        @SideOnly(Side.CLIENT)
        public IMessage onMessage(DeleteEntityMessage message, MessageContext ctx) {
            try {
                World world = FMLClientHandler.instance().getClient().world;
                Entity p = world.getEntityByID(message.playerId);
                world.removeEntity(p);
                System.out.println("Entity destroyed");
            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }
    }
}