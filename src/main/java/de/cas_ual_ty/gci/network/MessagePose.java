package de.cas_ual_ty.gci.network;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jna.platform.win32.WinBase;
import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.client.EventHandlerClient;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.lang.reflect.Type;
import java.util.*;


public class MessagePose implements IMessage{

    public String jsonList;

    public MessagePose()
    {

    }

    public MessagePose(String jsonList)
    {
        this.jsonList = jsonList;
    }

    @Override
    public void fromBytes(ByteBuf buf)
    {
        this.jsonList  = ByteBufUtils.readUTF8String(buf);
//        System.out.println("Read buf: " + jsonList);
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        ByteBufUtils.writeUTF8String(buf, this.jsonList);
    }

    public static class MessageHandlerPose implements IMessageHandler<MessagePose, IMessage>
    {
        @SideOnly(Side.CLIENT)
        @Override
        public IMessage onMessage(MessagePose message, MessageContext ctx)
        {
            EntityPlayer player = GunCus.proxy.getClientPlayer(ctx);

            if(player != null) {

                World world = FMLClientHandler.instance().getClient().world;
                if (world == null) return null;
                String jsonList = message.jsonList;
//                    System.out.println("jsonList " + jsonList);
                Gson gson = new Gson();
                ArrayList<String> list = gson.fromJson(jsonList, new TypeToken<ArrayList<String>>() {
                }.getType());
//                    System.out.println(list);
                //EventHandlerClient.isAimingClientList;
                EventHandlerClient.isAimingClientList = list;
//                    System.out.println(EventHandlerClient.isAimingClientList);
                //Scales
                /*if (EventHandlerClient.TESTING1 == null) {*/
//                EventHandlerClient.TESTING1 = new NBTTagCompound();
//                Collection<NetworkPlayerInfo> list1 = Minecraft.getMinecraft().player.connection.getPlayerInfoMap();
////                        System.out.println(list1);
//                Iterator<NetworkPlayerInfo> iterlist1 = list1.iterator();
//
//                while (iterlist1.hasNext()) {
//                    NetworkPlayerInfo playerInfo = iterlist1.next();
//                    /*Можно убрать проверку но мб будет больше нагрузка*/
//                    if (!EventHandlerClient.TESTING1.hasKey(playerInfo.getGameProfile().getName())) {
//                        try {
//                            EventHandlerClient.TESTING1.setTag(playerInfo.getGameProfile().getName(), Objects.requireNonNull(EventHandlerClient.loadPlayerData(playerInfo.getGameProfile().getId())));
//                        } catch (NullPointerException e) {
//                            System.out.println("null");
//                        }
//                    }
//                }
//                if (!EventHandlerClient.TESTING1.hasKey(player.getName())) {
//                    try {
//                        EventHandlerClient.TESTING1.setTag(player.getName(), Objects.requireNonNull(EventHandlerClient.loadPlayerData(player.getUniqueID())));
//                    } catch (NullPointerException e){
//                            System.out.println("null");
//                    }
//                }

/*                    } else {
                        Collection<NetworkPlayerInfo> list1 = Minecraft.getMinecraft().player.connection.getPlayerInfoMap();
                        for (NetworkPlayerInfo playerInfo : list1) {
                            if (!EventHandlerClient.TESTING1.hasKey(playerInfo.getGameProfile().getName())) {
                                EventHandlerClient.TESTING1.setTag(playerInfo.getGameProfile().getName(), Objects.requireNonNull(EventHandlerClient.loadPlayerData(playerInfo.getGameProfile().getId())));
                            }
                        }
                    }*/
//                    System.out.println(EventHandlerClient.TESTING1);

            }
            return null;
        }
    }
}
