package de.cas_ual_ty.gci;


import de.cas_ual_ty.gci.network.OpenContainerMessage;
import de.cas_ual_ty.gci.network.OpenInventoryMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.server.MinecraftServer;
import org.lwjgl.input.Keyboard;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;

public class KeyHandler {

    /*Создаем бинд. Указываем название - Open Inventory
       Кнопка поумолчанию, если игрок ничего не менял будет H
       Название категории - Custom Inventory Keys*/
    public static KeyBinding openKey = new KeyBinding("Open Inventory", Keyboard.KEY_J, "Kmrp Keys");
    public static KeyBinding toggleKey = new KeyBinding("Toggle Toggleable", Keyboard.KEY_N, "Kmrp Keys");
    public static KeyBinding gunCusKey = new KeyBinding("Open Attachments Table", Keyboard.KEY_U, "Kmrp Keys");
    public static KeyBinding aimKey = new KeyBinding("Toggle Aim", Keyboard.KEY_I, "Kmrp Keys");
    public static KeyBinding toggleWeaponTag = new KeyBinding("Toggle Weapon Tag", Keyboard.KEY_M, "Kmrp Keys");
    /*public static KeyBinding containerKey = new KeyBinding("Open Container", Keyboard.KEY_J, "Custom Inventory Keys");*/

    //Регистрируем событие и бинд
    public static void register() {
        MinecraftForge.EVENT_BUS.register(new KeyHandler());
        ClientRegistry.registerKeyBinding(openKey);
        ClientRegistry.registerKeyBinding(toggleKey);
        ClientRegistry.registerKeyBinding(gunCusKey);
        ClientRegistry.registerKeyBinding(aimKey);
        ClientRegistry.registerKeyBinding(toggleWeaponTag);
        /*ClientRegistry.registerKeyBinding(containerKey);*/
    }

    //Событие, которое срабатывает при нажатии игроком кнопки на клавиатуре
    @SubscribeEvent
    public void onKey(KeyInputEvent event) {
        //если нажали на нашу кнопку Н то отправляем пакет на сервер с просьбой открыть инвентарь
        if (openKey.isPressed()) {
            GunCus.channel.sendToServer(new OpenInventoryMessage());
        } else if (toggleKey.isPressed()) {
            /*System.out.println("test");*/
            Minecraft.getMinecraft().player.sendChatMessage("/guncus toggleub");
            Minecraft.getMinecraft().player.sendChatMessage("/guncus togglers");
            Minecraft.getMinecraft().player.sendChatMessage("/guncus togglels");
            Minecraft.getMinecraft().player.sendChatMessage("/guncus togglestock");
            Minecraft.getMinecraft().player.sendChatMessage("/guncus toggleta");
        } else if (gunCusKey.isPressed()) {
            Minecraft.getMinecraft().player.sendChatMessage("/guncus gui gun");
        } else if (aimKey.isPressed()) {
            Minecraft.getMinecraft().player.sendChatMessage("/guncus toggleaim");
        } else if (toggleWeaponTag.isPressed()) {
            Minecraft.getMinecraft().player.sendChatMessage("/weapontag change");
        }

    }

}


