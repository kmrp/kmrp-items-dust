package de.cas_ual_ty.gci.client.render.layers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;

public class LayersRegister
{
    public static void register()
    {
        setLayer(new BackLayer());
        setLayer(new RightShoulderLayer());
        setLayer(new LeftShoulderLayer());
        setLayer(new RightHipLayer());
        setLayer(new LeftHipLayer());
    }

    private static void setLayer(final LayerRenderer layer)
    {
        Minecraft.getMinecraft().getRenderManager().getSkinMap().get("default").addLayer(layer);
        Minecraft.getMinecraft().getRenderManager().getSkinMap().get("slim").addLayer(layer);
    }
}
