package de.cas_ual_ty.gci;

import com.google.gson.Gson;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.ItemToggleable;
import de.cas_ual_ty.gci.item.attachment.*;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeable;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeableFood;
import de.cas_ual_ty.gci.network.MessagePose;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static de.cas_ual_ty.gci.client.ProxyClient.mpmdir;

//KM
public class GunCusCommand extends CommandBase {
    private boolean debugAiming = false;
    public static ArrayList<String> isAimingServerList = new ArrayList<String>();

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    public static void sendAimingListToEveryPlayer() {
        List<EntityPlayerMP> playerMPList = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers();
        Gson gson = new Gson();
        String jsonList = gson.toJson(isAimingServerList);
        /*System.out.println("sendAimingListToEveryPlayer " + jsonList);*/
        for (EntityPlayerMP playerMP : playerMPList) {
            GunCus.channel.sendTo(new MessagePose(jsonList), playerMP);
        }
    }
    //test
    private static NBTTagCompound loadPlayerData(java.util.UUID id){
        String filename = id.toString();
        if(filename.isEmpty())
            filename = "noplayername";
        filename += ".dat";

        try {
            File file = new File(mpmdir, filename);
            if(!file.exists()){
                return null;
            }
            return CompressedStreamTools.readCompressed(new FileInputStream(file));
        } catch (Exception e) {

        }
        try {
            File file = new File(mpmdir, filename+"_old");
            if(!file.exists()){
                return null;
            }
            return CompressedStreamTools.readCompressed(new FileInputStream(file));

        } catch (Exception e) {

        }
        return null;
    }

    public boolean isAiming (EntityPlayerMP player) {
        return isAimingServerList.contains(player.getName());
    }

    @Override
    public String getName() {
        return "guncus";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }

        EntityPlayerMP player = getCommandSenderAsPlayer(sender);
        ItemStack itemStack;
        ItemGun gun;
        int attachmentID;
        switch (args[0]) {
            case "gui":
            case "guns":

                player.openGui(GunCus.instance, 0, player.world, player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ());
                return;
            case "armor":
                player.openGui(GunCus.instance, 1, player.world, player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ());
                return;

            case "toggleub":
                itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();
                if (itemStack.getItem() instanceof ItemGun) {
                    gun = (ItemGun) itemStack.getItem();
                    Underbarrel ub = (Underbarrel) gun.getAttachment(itemStack, 3);
                    if (ub != null && ub.getToggleableTo() != null) {
                        gun.setAttachment(itemStack, 3, ub.getToggleableTo());
                    }
                }
                return;
            case "togglers":
                itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();
                if (itemStack.getItem() instanceof ItemGun) {
                    gun = (ItemGun) itemStack.getItem();
                    Ammo rs = (Ammo) gun.getAttachment(itemStack, 5);
                    if (rs != null && rs.getToggleableTo() != null) {
                        gun.setAttachment(itemStack, 5, rs.getToggleableTo());
                    }
                }
                return;
            case "togglels":
                itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();
                if (itemStack.getItem() instanceof ItemGun) {
                    gun = (ItemGun) itemStack.getItem();
                    Auxiliary ls = (Auxiliary) gun.getAttachment(itemStack, 4);
                    if (ls != null && ls.getToggleableTo() != null) {
                        gun.setAttachment(itemStack, 4, ls.getToggleableTo());
                    }
                }
                return;
            case "togglestock":
                itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();
                if (itemStack.getItem() instanceof ItemGun) {
                    gun = (ItemGun) itemStack.getItem();
                    Accessory stock = (Accessory) gun.getAttachment(itemStack, 1);
                    if (stock != null && stock.getToggleableTo() != null) {
                        gun.setAttachment(itemStack, 1, stock.getToggleableTo());
                    }
                }
                return;
            case "toggleaim":
               /* System.out.println(player.getName());*/
                String name = player.getName();
                if (isAimingServerList.contains(name)) {
                    isAimingServerList.remove(name);
                } else {
                    isAimingServerList.add(name);
                }
                // send list to clients
                sendAimingListToEveryPlayer();
                return;

            case "setcontainer":
                ItemStack is = player.getHeldItemMainhand();
                if (is.getCount() > 1) {
                    return;
                }
                Date now = new Date();
                SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
                NBTTagString id = new NBTTagString(player.getName() + format.format(now));
                if (is.getTagCompound() == null) { is.setTagCompound(new NBTTagCompound()); }
                is.getTagCompound().setTag("mediumcontainer", id);
                is.getItem().setMaxStackSize(1);
                /*System.out.println("id " + is.getTagCompound().getString("mediumcontainer"));*/
                return;
            case "setbigcontainer":
                ItemStack is1 = player.getHeldItemMainhand();
                if (is1.getCount() > 1) {
                    return;
                }
                Date now1 = new Date();
                SimpleDateFormat format1 = new SimpleDateFormat("ddMMyyyyHHmmss");
                NBTTagString id1 = new NBTTagString(player.getName() + format1.format(now1));
                if (is1.getTagCompound() == null) { is1.setTagCompound(new NBTTagCompound()); }
                is1.getTagCompound().setTag("bigcontainer", id1);
                is1.getItem().setMaxStackSize(1);
                /*System.out.println("id " + is1.getTagCompound().getString("bigcontainer"));*/
                return;
            case "dye":
                int color = Integer.parseInt(args[1].toUpperCase(), 16);

                ItemStack itemStack1 = player.getHeldItemMainhand();
                if (itemStack1.getItem() instanceof ItemDyeable || itemStack1.getItem() instanceof ItemDyeableFood) {
                    NBTTagInt nbtTagInt = new NBTTagInt(color);
                    if (itemStack1.getTagCompound() == null) {
                        itemStack1.setTagCompound(new NBTTagCompound());
                    }
                    itemStack1.getTagCompound().setTag("itemDyeableColor", nbtTagInt);
                }
                return;
/*            case "test":
                List<EntityPlayerMP> playerMPList = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers();
                for (EntityPlayerMP playerMP : playerMPList) {
                    System.out.println(loadPlayerData(playerMP.getUniqueID()));
                }
                System.out.println("test");
                return;*/

            case "toggleta":
                ItemStack is2 = player.getHeldItemMainhand();
                if (is2.getItem() instanceof ItemToggleable) {
                    String toggleableTo = ((ItemToggleable) is2.getItem()).getToggleableTo();
                    NBTTagCompound tagCompound = is2.getTagCompound();
                    Item ite1 = ForgeRegistries.ITEMS.getValue(new ResourceLocation(GunCus.MOD_ID + ":" + toggleableTo));
                    ItemStack toggleableToIs = null;
                    /*System.out.println("test1");*/
                    if (ite1 != null) {
                        toggleableToIs = new ItemStack(ite1);
                        toggleableToIs.setStackDisplayName(is2.getDisplayName());
                        toggleableToIs.setTagCompound(tagCompound);
                        player.inventory.setInventorySlotContents(player.inventory.currentItem, toggleableToIs);
                        /*System.out.println("test2");*/
                    }
                } else if (is2.getItem() instanceof Attachment) {
                    Integer toggleableToId = null;
                    String toggleableToRl = null;
                    if (is2.getItem() instanceof Accessory) { toggleableToId = ((Accessory) is2.getItem()).getToggleableTo(); }
                    if (is2.getItem() instanceof Optic) { toggleableToId = ((Optic) is2.getItem()).getToggleableTo(); }
                    if (is2.getItem() instanceof Auxiliary) { toggleableToId = ((Auxiliary) is2.getItem()).getToggleableTo(); }
                    if (is2.getItem() instanceof Ammo) { toggleableToId = ((Ammo) is2.getItem()).getToggleableTo(); }
                    if (is2.getItem() instanceof Underbarrel) { toggleableToId = ((Underbarrel) is2.getItem()).getToggleableTo(); }


                    if (toggleableToId != null) {
                        Item toggleableToItem = Attachment.getAttachment(((Attachment) is2.getItem()).getSlot(), toggleableToId);
                        ItemStack toggleableToIs = new ItemStack(toggleableToItem);
                        NBTTagCompound tagCompound = is2.getTagCompound();
                        toggleableToIs.setStackDisplayName(is2.getDisplayName());
                        toggleableToIs.setTagCompound(tagCompound);
                        player.inventory.setInventorySlotContents(player.inventory.currentItem, toggleableToIs);
                    } else if (toggleableToRl != null) {
                        NBTTagCompound tagCompound = is2.getTagCompound();
                        Item ite1 = ForgeRegistries.ITEMS.getValue(new ResourceLocation(GunCus.MOD_ID + ":" + toggleableToRl));
                        ItemStack toggleableToIs = null;
                        if (ite1 != null) {
                            toggleableToIs = new ItemStack(ite1);
                            toggleableToIs.setStackDisplayName(is2.getDisplayName());
                            toggleableToIs.setTagCompound(tagCompound);
                            player.inventory.setInventorySlotContents(player.inventory.currentItem, toggleableToIs);
                        }
                    }

                }
                return;
            case "transferta":
                ItemStack is3 = player.getHeldItemMainhand();
                if (is3.getItem() instanceof Attachment) {
                    String transferableToRl = null;
                    if (is3.getItem() instanceof Underbarrel) {
                        transferableToRl = ((Underbarrel) is3.getItem()).getTransferableToRl();
                    }
                    if (is3.getItem() instanceof Magazine) {
                        transferableToRl = ((Magazine) is3.getItem()).getTransferableToRl();
                    }
                    if (is3.getItem() instanceof Accessory) {
                        transferableToRl = ((Accessory) is3.getItem()).getTransferableToRl();
                    }
                    if (is3.getItem() instanceof Auxiliary) {
                        transferableToRl = ((Auxiliary) is3.getItem()).getTransferableToRl();
                    }
                    if (is3.getItem() instanceof Ammo) {
                        transferableToRl = ((Ammo) is3.getItem()).getTransferableToRl();
                    }

                    if (transferableToRl != null) {
                        NBTTagCompound tagCompound = is3.getTagCompound();
                        Item ite1 = ForgeRegistries.ITEMS.getValue(new ResourceLocation(GunCus.MOD_ID + ":" + transferableToRl));
                        ItemStack toggleableToIs = null;
                        if (ite1 != null) {
                            toggleableToIs = new ItemStack(ite1);
                            toggleableToIs.setStackDisplayName(is3.getDisplayName());
                            toggleableToIs.setTagCompound(tagCompound);
                            player.inventory.setInventorySlotContents(player.inventory.currentItem, toggleableToIs);
                        }
                    }
                }
                return;
            case "toggleoptic":
                itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();
                if (itemStack.getItem() instanceof ItemGun) {
                    gun = (ItemGun) itemStack.getItem();
                    Optic optic = (Optic) gun.getAttachment(itemStack, 0);
                    if (optic != null && optic.getToggleableTo() != null) {
                        gun.setAttachment(itemStack, 0, optic.getToggleableTo());
                    }
                }
                return;

        }
    }
}
// RP